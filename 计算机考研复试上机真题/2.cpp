#include <iostream>
#include <cstdlib>
#include <cstdio>
using namespace std;

void output(string s)
{
	for(int i=0;i<s.length();i++)
	{
		if(s[i]>='a'&&s[i]<='z'||s[i]>='A'&&s[i]<='Z')
		{
			if(s[i]=='z')
			{
				cout<<'a';
			}
			else if(s[i]=='Z')
			{
				cout<<'A'; 
			}
			else
			{
				cout<<(char )(s[i]+1);
			}
		}
		else
		{
			cout<<s[i];
		}
	}
	return ;
} 

int main()
{
	string s;
	int n;
	while(cin>>n)
	{
		getchar();
		for(int i=0;i<n;i++)
		{
			getline(cin,s,'\n');
			output(s);
		}
	}	
	return 0;
} 
