#include <iostream>
#include <algorithm>
using namespace std;

struct student
{
	int id;
	int score;
};

bool cmp(const student & a ,const student & b )
{
	if(a.score==b.score)
	{
		return a.id<b.id;
	}
	else
	{
		return a.score<b.score;
	}
}

int main()
{
	student ans[110];
	int n;
	cin>>n;
	for(int i=0;i<n;i++)
	{
		cin>>ans[i].id>>ans[i].score;
	}
	sort(ans,ans+n,cmp);
	for(int i=0;i<n;i++)
	{
		cout<<ans[i].id<<" "<<ans[i].score<<endl;
	}
	return 0;
}
