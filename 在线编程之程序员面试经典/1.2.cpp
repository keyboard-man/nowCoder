class Reverse {
public:
    string reverseString(string iniString) {
        // write code here
        char temp;
        int i=0;
        int j=iniString.length()-1;
        while(i<j)
        {
            temp=iniString[i];
            iniString[i]=iniString[j];
            iniString[j]=temp;
            i++;
            j--;
        }
        return iniString;
    }
};
