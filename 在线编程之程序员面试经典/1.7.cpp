class Clearer {
public:
    vector<vector<int> > clearZero(vector<vector<int> > mat, int n) {
        // write code here
        int x[310],y[310];
        for(int i=0;i<310;i++)
        {
        	x[i]=0;
        	y[i]=0;
        }
        for(int i=0;i<n;i++)
        {
        	for(int j=0;j<n;j++)
        	{
        		if(mat[i][j]==0)
        		{
        			x[i]=1;
        			y[j]=1;
        		}
        	}
        }
        for(int i=0;i<n;i++)
        {
        	if(x[i]==1)
        	{
        		int xx=0;
        		while(xx<n)
        		{
        			mat[i][xx]=0;
        			xx++;
        		}
        	}
        	if(y[i]==1)
        	{
        		int yy=0;
        		while(yy<n)
        		{
        			mat[yy][i]=0;
        		}
        	}
        }
        return mat; 
    }
};
