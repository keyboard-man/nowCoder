
class Same {
public:
    bool checkSam(string stringA, string stringB) {
        // write code here
        if(stringA.length()!=stringB.length()) return false;
        int a[2][256];
        memset(a,0,sizeof(a));
        for(int i=0;i<stringA.length();i++)
        {
        	a[0][(int)(stringA[i])]++;
        	a[1][(int)(stringB[i])]++;
        }
        for(int i=0;i<256;i++)
        {
        	if(a[0][i]!=a[1][i]) return false;
        }
        return true;
    }
};

