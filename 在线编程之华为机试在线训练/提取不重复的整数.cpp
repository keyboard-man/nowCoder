#include <iostream>
using namespace std;

int main()
{
	string str;
	cin>>str;
	int index=str.length()-1;
	while(index>0)
	{
		if(str[index]>='0'&&str[index]<='9')
		{
			int pos=str.find_first_of(str[index]);
			while(pos>=0&&pos!=index)
			{
				str[pos]='A';
				pos=-1;
				pos=str.find_first_of(str[index]);
			}
		}
		index--;
	}
	for(int i=str.length()-1;i>=0;i--)
	{
		if(str[i]!='A')
		{
			cout<<str[i];
		}
	}
	cout<<endl;
	return 0;
} 
