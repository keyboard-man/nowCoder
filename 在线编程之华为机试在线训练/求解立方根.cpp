#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;
int main()
{
	double ans;
	while(cin>>ans)
	{
		cout<<setiosflags(ios::fixed)<<setprecision(1)<<pow(ans,1.0/3)<<endl;
	}
} 
