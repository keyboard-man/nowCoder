#include <iostream>
using namespace std;
#define SIZE 100
int ans[SIZE][SIZE];
void init()
{
	ans[0][0]=1;
	int num=1;
	for(int i=1;i<100;i++)
	{
		ans[i][0]=ans[i-1][0]+num;
		num++;
	}
	num=2;
	for(int i=0;i<100;i++)
	{
		int copy=num;
		for(int j=1;j<100;j++)
		{
			ans[i][j]=ans[i][j-1]+copy;
			copy++;
		}
		num++;
	}
	return ;
}
int main()
{
	init();
	int n;
	while(cin>>n)
	{
		int n_copy=n;
		for(int i=0;i<n;i++)
		{
			for(int j=0;j<n_copy;j++)
			{
				if(j+1==n_copy) cout<<ans[i][j]<<endl;
				else cout<<ans[i][j]<<" ";
			}
			n_copy--;
		}
	}
	return 0;
} 
