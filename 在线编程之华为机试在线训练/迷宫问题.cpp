#include <iostream>
#include <list>
#include <cstring>
using namespace std;
#define SIZE 100
int maze[SIZE][SIZE];
int flag[SIZE][SIZE];

struct point
{
	int x,y;
};
list<point> res;
int n,m;

int dir[4][2]={{0,1},{0,-1},{1,0},{-1,0}};

void get(int x,int y,int length)
{
	flag[x][y]=length;
	for(int i=0;i<4;i++)
	{
		int xx=x+dir[i][0];
		int yy=y+dir[i][1];
		if(xx>=0&&xx<n&&yy>=0&&yy<m&&maze[xx][yy]==0)
		{
			if(flag[xx][yy]==0)
			{
				//flag[xx][yy]=length+1;
				get(xx,yy,length+1);
			}
			else
			{
				flag[xx][yy]=flag[xx][yy]<(length+1)?flag[xx][yy]:(length+1);
			}
		}
	}
	return ;
}

void getres(int x,int y)
{
	if(x==0&&y==0)
	{
		return ;
	}
	for(int i=0;i<4;i++)
	{
		int xx=x+dir[i][0];
		int yy=y+dir[i][1];
		if(xx>=0&&xx<n&&yy>=0&&yy<m) 
		{
			if(flag[xx][yy]==flag[x][y]-1)
			{
				point temp;
				temp.x=xx;
				temp.y=yy;
				res.push_front(temp);
				getres(xx,yy);
				return ;
			}
		}
	}
	return ;
}

int main()
{
	while(cin>>n>>m)
	{
		memset(flag,0,sizeof(flag));
		flag[0][0]=1;
		for(int i=0;i<n;i++)
		{
			for(int j=0;j<m;j++) cin>>maze[i][j];
		}
		get(0,0,1);
		point temp;
		temp.x=n-1;
		temp.y=m-1;
		res.push_front(temp);
		getres(n-1,m-1);
		list<point>::iterator it=res.begin();
		while(it!=res.end())
		{
			cout<<"("<<it->x<<","<<it->y<<")"<<endl;
			it++;
		}
		res.clear();
	}
}
