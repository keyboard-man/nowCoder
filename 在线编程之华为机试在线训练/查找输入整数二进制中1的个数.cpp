#include <iostream>
#include <bitset>
using namespace std;

int main()
{
	int num;
	while(cin>>num)
	{
		bitset<32> ans(num);
		string str=ans.to_string();
		int max_len=0;
		for(int i=0;i<str.length();i++)
		{
			if(str[i]=='1')
			{
				int temp=0;
				while(str[i]=='1')
				{
					temp++;
					i++;
				}
				if(max_len<temp)
				{
					max_len=temp;
				}
			}
		} 
		cout<<max_len<<endl;
	}
	return 0;
}
