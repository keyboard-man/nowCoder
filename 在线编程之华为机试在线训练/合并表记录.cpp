#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;
struct data
{
	int index;
	int value;
	bool operator ==(const data b)
	{
		return index==b.index;
	}
};
vector<data> res;

bool cmp(const data & a,const data & b)
{
	return a.index<b.index;
}
int main() 
{
	int n;
	cin>>n;
	for(int i=0;i<n;i++)
	{
		data input;
		
		cin>>input.index>>input.value;
		vector<data>::iterator it=find(res.begin(),res.end(),input);
		if(it==res.end())
		{
			res.push_back(input);
		}
		else
		{
			it->value+=input.value;
		}
	}
	sort(res.begin(),res.end(),cmp);
	
	vector<data>::iterator it=res.begin();
	for(;it!=res.end();it++)
	{
		
		cout<<it->index<<" "<<it->value<<endl;
		
	}
	
	return 0;
}
