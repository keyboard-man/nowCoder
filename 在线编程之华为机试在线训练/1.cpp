#include <iostream> 
#include <cstdlib>
using namespace std;

struct IP
{
	int a,b,c,d;
	bool operator ==(const IP &ip )
	{
		return (a==ip.a&&b==ip.b&&c==ip.c&&d==ip.d);
	}
};

bool judge(string s ,IP & ip)
{
	int pos=0;
	ip.a=atoi(&s[pos]);
	if(ip.a>255||ip.a<0) return false;
	
	pos=s.find_first_of('.',pos);
	pos++;
	ip.b=atoi(&s[pos+1]);
	if(ip.b>255||ip.b<0) return false;
	
	pos=s.find_first_of('.',pos+1);
	ip.c=atoi(&s[pos+1]);
	if(ip.c>255||ip.c<0) return false;
	
	pos=s.find_first_of('.',pos+1);
	ip.d=atoi(&s[pos+1]);
	if(ip.d>255||ip.d<0)return false;
	return true;
	
}

int main()
{
	
	string s1;
	while(	cin>>s1 )
	{
		IP t1;
		if(judge(s1,t1))
		{
			cout<<t1.a<<endl<<t1.b<<endl<<t1.c<<endl<<t1.d<<endl; 
		 	cout<<"YES"<<endl; 
		}
		else
		{
			cout<<"NO"<<endl;
		}
	}

	return 0;
}
