#include <iostream> 
#include <cstdlib>
using namespace std;

int main()
{
	string str;
	while(getline(cin,str))
	{
		int char_num=0;
		int num=0;
		int b=0;
		int other=0;
		for(int i=0;i<str.length();i++)
		{
			if(str[i]>='A'&&str[i]<='Z'||str[i]>='a'&&str[i]<='z') ++char_num;
			else if(str[i]>='0'&&str[i]<='9') ++num;
			else if(str[i]==' ') ++b;
			else ++other;
		}
		cout<<char_num<<endl<<b<<endl<<num<<endl<<other<<endl;
	}
	return 0;
}
