#include <iostream>
#include <cstring>
#include <string>
using namespace std;
 
int ans[100000];
 
int main()
{
    string str;
    while(cin>>str)
    {
        memset(ans,0,sizeof(ans));
        string s=str;
        str.clear();
        str="#";
        for(int i=0;i<s.length();i++)
        {
            str=str+s[i]+"#";
        }
        ans[0]=1;
        int max_len=0;
        for(int i=1;i<str.length();i++)
        {
             
            int l=i-1,r=i+1;
            bool flag=true;
            int num;
            if(str[i]=='#') num=0;
            else num=1;
            while(l>=0&&r<str.length())
            {
                if(str[l]==str[r])
                {
                    if(str[l]!='#')num+=2;
                	l--;
                	r++;
                }
                else break;
                
            }
            ans[i]=num;
            max_len=max_len>num?max_len:num;
        }
        cout<<max_len<<endl;
    }
}
