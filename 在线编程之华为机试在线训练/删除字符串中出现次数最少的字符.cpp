#include <iostream>
#include <cstdlib>
#include <cstring>
using namespace std;

int main()
{
	string str;
	while(cin>>str)
	{
		unsigned char ans[25];
		memset(ans,0,sizeof(ans));
		for(int i=0;i<str.length();i++)
		{
			int num=1;
			for(int j=0;j<str.length();j++)
			{
				if(str[j]==str[i]&&j!=i)
				{
					num++; 
				}
			}
			ans[i]=num;
			for(int j=0;j<str.length();j++)
			{
				if(str[j]==str[i])
				{
					ans[j]=num; 
				}
			}
		}
		unsigned char  min_num=100;
		for(int i=0;i<str.length();i++)
		{
			min_num=min_num>ans[i]?ans[i]:min_num;
		}
		for(int i=0;i<str.length();i++)
		{
			if(ans[i]!=min_num) cout<<str[i];
		}
		cout<<endl;
	}
	return 0;
} 
