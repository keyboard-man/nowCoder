#include <iostream>
#include <vector>
#include <string>
#include <cstdio>
using namespace std;

int main()
{
	string str;
	while(getline(cin,str))
	{
		vector<string> ans;
		int pos=0;
		while(1)
		{
			if(str[pos]=='\"')
			{
				int pos2=str.find_first_of('\"',pos+1);
				string sub=str.substr(pos+1,pos2-pos-1);
				ans.push_back(sub);
				if(pos2==str.length()-1)
				{
					break;
				}
				else
				{
					pos=pos2+2;
				}
			}
			else
			{
				int pos2=str.find_first_of(' ',pos);
				string sub;
				if(pos2==string::npos)
				{
					sub=str.substr(pos);
					ans.push_back(sub);
					break;
				}
				else
				{
					sub=str.substr(pos,pos2-pos);
					ans.push_back(sub);
					pos=pos2+1;
				}
			}
		}
		cout<<ans.size()<<endl;
		for(int i=0;i<ans.size();i++)
		{
		 	cout<<ans[i]<<endl; 
		}
		//getchar();
	}
} 
