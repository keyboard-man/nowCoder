#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

int main()
{
	int n,m;
	while(cin>>n>>m)
	{
		vector<int> res;
		int num;
		for(int i=0;i<n;i++)
		{
			cin>>num;
			res.push_back(num);
		}
		sort(res.begin(),res.end());
		for(int i=0;i<m-1;i++)
			cout<<res[i]<<" ";
		cout<<res[m-1]; 
	}
	return 0;
}
