#include <iostream>
#include <algorithm>
using namespace std;

int ans[1000000];
bool cmp(const int & a,const int & b)
{
	return a>b;
}
int main() 
{
	int n;
	while(cin>>n)
	{
		for(int i=0;i<n;i++) cin>>ans[i];
		int k;
		cin>>k;
		if(k==0)
		{
			sort(ans,ans+n);
		} 
		else
		{
			sort(ans,ans+n,cmp);
		}
		for(int i=0;i<n-1;i++) cout<<ans[i]<<" ";
		cout<<ans[n-1]<<endl;
	} 
}
