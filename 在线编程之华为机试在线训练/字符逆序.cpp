#include <iostream>
#include <vector>
#include <string>
using namespace std;

string reverse(string str) 
{
	int i=0,j=str.length()-1;
	while(i<j)
	{
		char ch=str[i];
		str[i]=str[j];
		str[j]=ch;
		i++;
		j--;
	}
	return str;
}

int main()
{
	string str;
	while(getline(cin,str))
	{
		vector<string> ans;
		int pos1,pos2;
		pos1=0;
		while(1)
		{
			pos2=str.find_first_of(' ',pos1+1);
			if(pos2==string::npos)
			{
				string temp=str.substr(pos1);
				ans.push_back(temp);
				break;
			}
			else
			{
				string temp=str.substr(pos1,pos2-pos1);
				ans.push_back(temp);
				pos1=pos2+1;
			}
		}
		for(int j=ans.size()-1;j>0;j--)
		{
			cout<<reverse(ans[j])<<" ";
		}
		cout<<reverse(ans[0])<<endl;
	}
}
