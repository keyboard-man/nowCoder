#include <iostream>
#include <algorithm>
#include <vector> 
using namespace std;

struct data
{
	char ch;
	int num;
	bool operator == (const data & k)
	{
		return k.ch==ch;
	}
};

vector<data> ans;

bool cmp(const data & a,const data & b)
{
	if(a.num==b.num) return a.ch<b.ch;
	else return a.num>b.num;
}

int main()
{
	string str;
	while(getline(cin,str))
	{
		for(int i=0;i<str.length();i++)
		{
			if(str[i]>='a'&&str[i]<='z'||str[i]>='A'&&str[i]<='Z'||str[i]==' '||str[i]>='0'&&str[i]<='9')
			{
				data t;
				t.ch=str[i];
				t.num=1;
				vector<data>::iterator it=std::find(ans.begin(),ans.end(),t);
				if(it==ans.end())
				{
					ans.push_back(t);
				}
				else
				{
					it->num++;
				}
			}
			else
			{
				
			}
			
		}
		sort(ans.begin(),ans.end(),cmp);
		vector<data>::iterator it=ans.begin();
		//cout<<ans.size()<<endl;
		while(it!=ans.end())
		{
			cout<<it->ch;
			it++; 
		}
		cout<<endl;
		ans.clear();
	}
}
