#include <iostream>
using namespace std;

long long gcd(long long  a,long long b)
{
	if(a<b)
	{
		a=a+b;
		b=a-b;
		a=a-b;
	}
	while(b>0)
	{
		int r=a%b;
		a=b;
		b=r;
	}
	return a;
}


int main()
{
	long long a,b;
	while(cin>>a>>b)
	{
		cout<<a*b/gcd(a,b)<<endl;
	}
}
