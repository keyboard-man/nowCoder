#include <iostream>
#include <vector> 
#include <queue>
#include <list>
#include <algorithm>
using namespace std;

struct data
{
	string file;
	int num;
	bool operator ==(const data & other)
	{
		return file==other.file;
	}
};
list<data> ans;
void insertData(string str)
{
	data tmp_data;
	tmp_data.file=str;
	list<data>::iterator it=find(ans.begin(),ans.end(),tmp_data);
	if(it==ans.end())
	{
		tmp_data.num=1;
		if(ans.size()<8)
		{
			ans.push_back(tmp_data);
		}
		else
		{
			ans.pop_front();
			ans.push_back(tmp_data);
		}
		
	}
	else
	{
		it->num+=1;
	}
}
bool cmp (data & a,data & b)
{
	return true;
}
int main()
{
	string str;
	while(getline(cin,str)&&str!="")
	{
		int pos=str.find_last_of('\\');
		int pos2=str.find_last_of(' ');
		if(pos<0)
		{
			if(pos2<=16)
			{
				insertData(str);
			}
			else
			{
				str=str.substr(pos2-16);
				insertData(str);
			}
		}
		else
		{
			string tmp=str.substr(pos+1);
			if((pos2-pos)<=16)
			{
				insertData(tmp);
			}
			else
			{
				tmp=str.substr(pos2-16);
				insertData(tmp);
			}
		}
	}
	list<data>::iterator it=ans.begin();
	for(;it!=ans.end();++it)
	{
		cout<<it->file<<" "<<it->num<<endl;
	}
	
	
	return 0;
}
