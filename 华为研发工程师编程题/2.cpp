#include <iostream>
#include <set>
using namespace std;

int main()
{
	set<int> ans;
	int n;
	while(cin>>n)
	{
		for(int i=0;i<n;i++)
		{
			int num;
			cin>>num;
			ans.insert(num);
		}
		set<int>::iterator it=ans.begin();
		for(;it!=ans.end();it++)
		{
			cout<<*it<<endl;
		}
		ans.clear();
	} 
	
	return 0;
}
