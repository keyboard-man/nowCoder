#include <iostream>
using namespace std;

long long gcd(long long  a,long long  b)
{
	if(a<b)
	{
		long long temp=a;
		a=b;
		b=temp;
	}
	while(b>0)
	{
		int temp =a%b;
		a= b;
		b=temp;
	}
	return a;
}

int main()
{
	int n,m;
	while(cin>>n>>m)
	{
		long long ans=m;
		for(int i=0;i<n;i++)
		{
			long long input;
			cin>>input;
			if(input<=ans)
			{
				ans+=input;
			}
			else
			{
				ans+=gcd(ans,input);
			}
		}
		cout<<ans<<endl;
	}
	return 0;
} 
